
var mongoose = require("mongoose");
console.log('mongoose.version=' + mongoose.version);

var userSchema = mongoose.Schema({
    firstName:String,
    lastName:String,
    userId:String,
    password:String,
    id:String
});

var User = mongoose.model('User', userSchema);

module.exports = User;
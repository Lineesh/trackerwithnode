
var mongoose = require("mongoose");

var activitySchema = mongoose.Schema({
    id: String,
    userId:String,
    type:String,
    date:Date,
    unit:String,
    count:Number,
    comment:String
});

var Activity = mongoose.model('Activity', activitySchema);

module.exports = Activity;
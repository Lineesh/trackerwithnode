//setup mongoose connection
var mongoose = require("mongoose");

var db = mongoose.createConnection('mongodb://localhost/data');

db.on('error', function(err){
  if(err) throw err;
});

db.once('open', function callback () {
  console.info('Mongo db connected successfully');
});

module.exports = db;

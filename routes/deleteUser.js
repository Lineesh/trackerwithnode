var express = require('express');
var router = express.Router();

//to create the schema
var UserDummy = require('./../models/User');
var db = require('./../models/db');
var User = db.model('User');

router.get('/', function(req, res, next) {
    var idToDelete = req.param('id');
    var userToDelete = User.find({_id:idToDelete});
    userToDelete.remove(function(err) {console.log('error=' + err)});
    
    res.redirect('/listUser');
});

module.exports = router;
var express = require('express');
var router = express.Router();

//to create the schema
var ActivityDummy = require('./../models/Activity');
var db = require('./../models/db');
var Activity = db.model('Activity');

var UserDummy = require('./../models/User');
var User = db.model('User');

var exerciseTypes = ['running', 'walking', 'push up', 'sit up', 'dumbbell'];

router.get('/', function(req, res, next) {
    User.find(function(err, usersFromDb) {
        if (err) {
            console.log('***trackActivity.find error=' + err);
        } else {
            res.render('trackActivity', {users:usersFromDb, types:exerciseTypes});
        }
    });
    
});


function trackActivityMiddle(req, res, next) {
    var userId = req.body.userId;
    var type = req.body.type;
    var date = req.body.date;
    var unit = req.body.unit;
    var count = req.body.count;
    var comment = req.body.comment;
    req.assert('date', 'Date is required').notEmpty(); 
    req.assert('unit', 'Unit is required').notEmpty(); 
    req.assert('count', 'Count is required').notEmpty(); 
    req.assert('comment', 'Comment is required').notEmpty(); 

    var errors = req.validationErrors(); 
    
    if (errors) {
        req.errors = errors;
    } else {
        var activity = new Activity({
            userId : userId,
            type : type,
            date : date,
            unit : unit,
            count: count,
            comment: comment
        });
        saveActivity(activity);
    }
    
    next();
}

function saveActivity(activity) {
    activity.save(function(err, model) {
        if (err) {
            console.log('***saveActivity error=' + err);
        } else {
            console.log('***saved activity***');
        }
    });
}   

router.post('/', trackActivityMiddle, function(req, res, next) {
    var userId = req.body.userId;
    var type = req.body.type;
    var date = req.body.date;
    var unit = req.body.unit;
    var count = req.body.count;
    var comment = req.body.comment;

    if (!req.errors) {
        res.render('index', {  });
    } else {
        User.find(function(err, usersFromDb) {
            if (err) {
                console.log('activity.find user error=' + err);
            } else {
                res.render('trackActivity', {errors:req.errors, userId:userId, type:type, date:date, unit:unit, count:count, comment:comment, users:usersFromDb, types:exerciseTypes});
            }
        });
    }
});

module.exports = router;

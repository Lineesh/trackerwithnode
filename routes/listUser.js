var express = require('express');
var router = express.Router();

//to create the schema
var UserDummy = require('./../models/User');
var db = require('./../models/db');
var User = db.model('User');

router.get('/', function(req, res) {
    User.find(function(err, users) {
        if (err) {
            console.log('***listUser.find error=' + err);
        } else {
            res.render('listUser', {users:users});
        }
    });
});

module.exports = router;
var express = require('express');
var router = express.Router();

//to create the schema
var UserDummy = require('./../models/User');
var db = require('./../models/db');
var User = db.model('User');

router.get('/', function(req, res, next) {
  res.render('createUser');
});

function createUserMiddle(req, res, next) {
    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var userId = req.body.userId;
    var password = req.body.password;
    req.assert('firstName', 'First name is required').notEmpty(); 
    req.assert('lastName', 'Last name is required').notEmpty(); 
    req.assert('userId', 'User id is required').notEmpty(); 
    req.assert('password', 'Password is required').notEmpty(); 

    var errors = req.validationErrors(); 
    
    if (errors) {
        req.errors = errors;
    } else {
        var user = new User({
            firstName : firstName,
            lastName : lastName,
            userId : userId,
            password : password
        });
        saveUser(user);
    }
    
    next();
}

function saveUser(user) {
    user.save(function(err, model) {
        if (err) {
            console.log('***error=' + err);
        } else {
            console.log('***saved user***');
        }
    });
}   

router.post('/', createUserMiddle, function(req, res, next) {
    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var userId = req.body.userId;
    var password = req.body.password;

    if (!req.errors) {
        res.render('index', {  });
    } else {
        res.render('createUser', {errors:req.errors, firstName:firstName, lastName:lastName, userId:userId, password:password});
    }
});

module.exports = router;
